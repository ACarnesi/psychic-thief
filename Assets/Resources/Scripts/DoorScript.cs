﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour {
	[SerializeField] Vector3 openPos, closedPos;
	[SerializeField] float speed;
	bool closing, opening, isOpen;

	void Update()
	{
		if(Time.timeScale == 1f)
		{
			if(closing)
			{
				if(transform.position != closedPos)
				{
					transform.position = Vector3.MoveTowards(transform.position, closedPos, Time.deltaTime * speed);
				}
				else
				{
					isOpen = false;
					closing = false;
				}
			}
			else if(opening)
			{
				if(transform.position != openPos)
				{
					transform.position = Vector3.MoveTowards(transform.position, openPos, Time.deltaTime * speed);
				}
				else
				{
					isOpen = true;
					opening = false;
				}
			}
		}
	}

	void OnCollisionEnter(Collision col)
	{
	if(closing && isOpen)
		{
			closing = false;
			opening = true;
		}
	}

	public void CycleOpenState()
	{
		if(isOpen)
			closing = true;
		else
			opening = true;
	}
}
