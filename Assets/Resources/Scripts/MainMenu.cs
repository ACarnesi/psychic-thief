﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class MainMenu : MonoBehaviour {
	

	void Start()
	{
		this.gameObject.SetActive(false);
		NotificationCenter.Default.AddObserver("ExitMenu", CloseMainMenu);
		NotificationCenter.Default.AddObserver("OpenMainMenu", OpenMainMenu);
	}

	public void OpenMainMenu(object o)
	{
		Time.timeScale = 0f;
		this.gameObject.SetActive(true);
	}

	public void CloseMainMenu(object o)
	{
		Time.timeScale = 1f;
		this.gameObject.SetActive(false);
	}

	public void ResumeClick()
	{
		NotificationCenter.Default.PostNotification("ExitMenu");
	}

	public void RestartClick()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void ExitClick()
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit();
		#endif
	}
		
	void OnDestroy()
	{
		NotificationCenter.Default.RemoveAllObservers();
	}
}

