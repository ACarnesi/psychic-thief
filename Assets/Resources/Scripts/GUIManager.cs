﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class GUIManager : MonoBehaviour {

	public Slider visibilitySlider, enduranceSlider; 
	public Image visibilitySliderFill, enduranceSliderFill, selectedAbility;
	public Text abilityText, informationText;
	public GameObject informationPanel;
	private float thisShade;

	PlayerVisibility playerVis;
	PsychicPowers playerPowers;
	FirstPersonController fpc;

	void Start()
	{
		playerVis = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerVisibility>();
		playerPowers = GameObject.FindGameObjectWithTag("Player").GetComponent<PsychicPowers>();
		fpc = GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>();
		visibilitySliderFill.color = Color.white;
		visibilitySlider.value = 0;
		enduranceSliderFill.color = Color.green;
		informationText.gameObject.SetActive(false);
		informationPanel.SetActive(false);
		Time.timeScale = 1.0f;
	}

	void FixedUpdate()
	{

		if(Input.GetKeyUp(KeyCode.Escape))
		{
			if(!fpc.InMenus)
			{
				NotificationCenter.Default.PostNotification("OpenMainMenu");
				Cursor.lockState = CursorLockMode.Confined;
				Cursor.visible = true;
			}
			else
			{
				NotificationCenter.Default.PostNotification("ExitMenu");
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
		}

		if(Time.timeScale == 1)
		{
			thisShade = Mathf.Lerp (visibilitySlider.value, playerVis.PlayerVis, Time.deltaTime * 2);
			if(!float.IsNaN(thisShade))
			{
				visibilitySlider.value = thisShade;
				enduranceSlider.value = playerPowers.Endurance;
				if (playerPowers.OverloadTime > 0)
					enduranceSliderFill.color = Color.red;
				else
					enduranceSliderFill.color = Color.green;
			}

		}
	}

	public void UpdateSelectedAbility(Sprite sprite, string s)
	{
		selectedAbility.sprite = sprite;
		abilityText.text = s;
	}
}
