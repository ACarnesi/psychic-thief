﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerInventory : MonoBehaviour {
	[SerializeField] private LayerMask playerLayer;

	bool handsFull;
	List<Item> itemList = new List<Item>();
	Camera cam; 
	GameObject lastObjectHit;

	void Start()
	{
		cam = GetComponentInChildren<Camera>();
	}

	void Update()
	{
		if(Time.timeScale == 1f)
		{
			RaycastHit hitInfo;
			Physics.Raycast (cam.transform.position, cam.transform.forward, out hitInfo, 2, playerLayer);
			if(hitInfo.collider != null)
			{
				if (hitInfo.collider.tag == "Item" || hitInfo.collider.tag == "DoorLock" || hitInfo.collider.tag == "SecurityKeypad" || hitInfo.collider.tag == "DataPad")
				{
					if(lastObjectHit != null)
					{
						if(lastObjectHit != hitInfo.collider.gameObject)
						{
							hitInfo.collider.gameObject.GetComponent<Renderer> ().material.shader = Shader.Find ("Self-Illumin/Bumped Diffuse");
							lastObjectHit.GetComponent<Renderer> ().material.shader = Shader.Find ("Standard");
							lastObjectHit = hitInfo.collider.gameObject;
						}
					}
					else
					{
						hitInfo.collider.gameObject.GetComponent<Renderer> ().material.shader = Shader.Find ("Self-Illumin/Bumped Diffuse");
						lastObjectHit = hitInfo.collider.gameObject;
					}
				}
				
				if(Input.GetKeyDown(KeyCode.E))
				{
					if(hitInfo.collider.tag == "Item")
					{
						AddItem(hitInfo.collider.GetComponent<Item>());
					}
					else if(hitInfo.collider.tag == "DoorLock")
					{
						DoorLock dL = hitInfo.collider.GetComponent<DoorLock>();
						dL.Interact(SearchInventory(dL.LockID));
					}
					else if(hitInfo.collider.tag == "SecurityKeypad")
					{
						SecurityKeypad sK = hitInfo.collider.GetComponent<SecurityKeypad> ();
						NotificationCenter.Default.PostNotification("OpenMenu");
						sK.Interact ();
					}
					if(hitInfo.collider.tag == "DataPad")
					{
						DataPad dP = hitInfo.collider.GetComponent<DataPad>();
						dP.Interact();
					}
				}
			}
			else
			{
				if(lastObjectHit != null)
				{
					lastObjectHit.GetComponent<Renderer> ().material.shader = Shader.Find ("Standard");
					lastObjectHit = null;
				}
			}
		}
	}

	public void AddItem(Item i)
	{
		itemList.Add(i);
		i.transform.position = transform.position;
		i.GetComponent<Renderer> ().enabled = false;
		i.GetComponent<Collider> ().enabled = false;;
		Debug.Log("Item Added");
	}

	public string SearchInventory(string ID)
	{
		foreach (Item i in itemList)
		{
			if(i.keyID == ID)
			{
				return i.keyID;
			}
			else
				continue;
		}
		return "";
	}
}
