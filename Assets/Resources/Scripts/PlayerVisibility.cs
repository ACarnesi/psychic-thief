﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerVisibility : MonoBehaviour {

	public List<Light> lightList = new List<Light>();
	public Flare flare;
	[SerializeField]private float playerVis;
	private Vector3 locationLastFrame;


	public float PlayerVis
	{
		get
		{
			return playerVis;
		}
		private set
		{
			playerVis = value;
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.timeScale == 1f)
		{
			PlayerVis = 0;
			float playerSpeed = Vector3.Distance (locationLastFrame, transform.position) / Time.deltaTime;
			locationLastFrame = transform.position;
			foreach(Light l in lightList)
			{
				Vector3 directionToBody, directionToHead;
				directionToBody = transform.position - l.transform.position;
				directionToHead = (new Vector3 (transform.position.x, transform.position.y + transform.localScale.y, transform.position.z) - l.transform.position);
				
				RaycastHit hitInfo;
				if(Physics.Raycast(l.transform.position, directionToBody, out hitInfo))
				{
					if(hitInfo.collider.tag == "Player")
					{
						float playerDistance = Vector3.Distance (transform.position, l.transform.position);
						PlayerVis += (l.range - playerDistance) * l.intensity;
					}
					
					else if(Physics.Raycast(l.transform.position, directionToHead, out hitInfo))
					{
						if(hitInfo.collider.tag == "Player")
						{
							float playerDistance = Vector3.Distance (transform.position, l.transform.position);
							PlayerVis += (l.range - playerDistance) * l.intensity;
						}
					}
				}
			}
			if (playerSpeed != 0)
				PlayerVis *= (playerSpeed * .5f + 1);
			if(PlayerVis > 255)
				PlayerVis = 255;
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "Light")
		{
			Light l = col.GetComponent<Light> ();
			lightList.Add (l);
			if(l.type == LightType.Spot)
			{
				l.flare = flare;
			}
		}
	}

	void OnTriggerExit(Collider col)
	{
		if(col.tag == "Light")
		{
			Light l = col.GetComponent<Light> ();
			lightList.Remove (l);
			if(l.type == LightType.Spot)
			{
				l.flare = null;
			}
		}
	}
}
