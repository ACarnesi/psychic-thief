﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class EndGoal : MonoBehaviour {

	// Restarts the Level upon trigger
	void OnTriggerEnter(Collider col){
		if(col.tag == "Player"){
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}
	}
}
