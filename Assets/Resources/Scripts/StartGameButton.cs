﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class StartGameButton : MonoBehaviour {

	public void StartGameClick()
	{
		SceneManager.LoadScene("Level 1");
	}
}
