﻿using UnityEngine;
using System.Collections;

public class DoorLock : MonoBehaviour {
	[SerializeField] DoorScript door;
	[SerializeField] string lockID;

	public string LockID
	{
		get{
			return lockID;
		}
	}

	public void Interact(string keyID)
	{
		if (lockID == "")
			door.CycleOpenState ();
		else if(keyID == lockID)
			door.CycleOpenState();
		else
			Debug.Log("ACESS DENIED: You do not have the required key.");
	}
}
