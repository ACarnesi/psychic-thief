﻿using UnityEngine;
using System.Collections;

public class AttackAnim : MonoBehaviour {

	GameObject blackJack;

	void Start()
	{
		blackJack = GameObject.FindGameObjectWithTag("BlackJack");
	}

	public void CheckForHit()
	{
		Ray r = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
		RaycastHit hitInfo;
		if(Physics.Raycast(r, out hitInfo, 2f))
		{
			if(hitInfo.collider.tag == "Enemy")
			{
				BasicEnemy bS = hitInfo.collider.GetComponent<BasicEnemy>();
				bS.Attacked();
			}
			else
			{
				//Make Sound if hit other object
			}
		}
	}
}
