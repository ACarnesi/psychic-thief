﻿using UnityEngine;
using System.Collections;

public class DataPad : MonoBehaviour {

	public string information;

	private GUIManager gui;

	// Use this for initialization
	void Start () {
		NotificationCenter.Default.AddObserver("ExitMenu", ExitMenu);
		gui = GameObject.FindGameObjectWithTag("GUI").GetComponent<GUIManager>();
	}

	public void ExitMenu(object o)
	{
		gui.informationText.gameObject.SetActive(false);
		gui.informationPanel.SetActive(false);
	}

	public void Interact()
	{
		gui.informationText.gameObject.SetActive(true);
		gui.informationPanel.SetActive(true);
		gui.informationText.text = information;
		NotificationCenter.Default.PostNotification("OpenMenu");
	}
}
