﻿using UnityEngine;
using System.Collections;

public class SecurityCamera : MonoBehaviour {

	[SerializeField]private Vector3 startEuler, endEuler;
	private Quaternion startRot, endRot;
	[SerializeField]private bool isRotating;
	[SerializeField]private float fieldOfView = 110;
	[SerializeField]private float speed;
	[SerializeField]private LayerMask enemyMask;
	private bool isActive;
	private float distToSeePlayer;
	private GameObject FoVObject;
	private Vector3 FoVScale;
	private PlayerVisibility playerVis; 
	private Transform player;


	void Start()
	{
		isActive = true;
		startRot = Quaternion.Euler(startEuler);
		endRot = Quaternion.Euler(endEuler);
		transform.rotation = startRot;
		StartCoroutine("RotationCycle");
		playerVis = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerVisibility>();
		player = GameObject.FindGameObjectWithTag ("Player").transform.GetChild (0);
		FoVObject = GameObject.FindGameObjectWithTag("FoV");
		FoVScale = FoVObject.transform.localScale;
	}

	void Update()
	{
		if(Time.timeScale == 1f)
			CheckForPlayer();
	}

	IEnumerator RotationCycle()
	{
		while(isActive)
		{
			if(isRotating)
			{
				while(transform.rotation != endRot)
				{
					transform.rotation = Quaternion.RotateTowards(transform.rotation, endRot, speed * Time.deltaTime);
					yield return null;
				}
				yield return new WaitForSeconds(3f);
				while(transform.rotation != startRot)
				{
					transform.rotation = Quaternion.RotateTowards(transform.rotation, startRot, speed * Time.deltaTime);
					yield return null;
				}
				yield return new WaitForSeconds(3f);
			}
			yield return null;
		}
		yield return null;
	}

	void CheckForPlayer()
	{
		distToSeePlayer = playerVis.PlayerVis / 10;
		if(Time.timeScale == 1)
		{
			Vector3 newFoVScale = Vector3.MoveTowards(FoVObject.transform.localScale, FoVScale * distToSeePlayer, Time.deltaTime*50);
			if(!float.IsNaN(newFoVScale.x))
				FoVObject.transform.localScale = newFoVScale;
		}
		float playerDist = Vector3.Distance(player.position, transform.position);
		//Check for player
		if(playerDist < distToSeePlayer)
		{
			Vector3 targetDir = player.position - transform.position;
			float angle = Vector3.Angle(transform.forward, targetDir);
			if(angle < fieldOfView * .5f)
			{
				RaycastHit hitInfo;
				if(Physics.Raycast(transform.position, targetDir, out hitInfo))
				{
					if(hitInfo.transform.tag == "Player")
					{
						Collider[] cols = Physics.OverlapSphere(transform.position, 30, enemyMask);
						foreach(Collider c in cols)
						{
							BasicEnemy enemy = c.GetComponent<BasicEnemy>();
							enemy.Alert();
						}
					}
				}
			}
		}
	}
}
