﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PsychicPowers : MonoBehaviour {
	[SerializeField]BasicEnemy activePercepTarget;
	[SerializeField]LayerMask enemyMask;
	[SerializeField]List<Renderer> FoVList = new List<Renderer>();
	[SerializeField]bool visionSight;
	[SerializeField] private LayerMask playerLayer;
	[SerializeField]Sprite hallucinateImage, activePreceptImage;

//	int hallucinateLevel, passivePercepLevel, activePercepLevel; //Not Implemented Yet
	GUIManager gui;
	Vector3 screenCenter;
	Camera cam;
	Animator attackAnim;
	int selectedPower;
	float endurance;
	float overloadTime;

	public float Endurance
	{
		get{return endurance;}
	}

	public float OverloadTime
	{
		get{return overloadTime;}
	}

	void Start()
	{
		attackAnim = GetComponentInChildren<Animator>();
		cam = GetComponentInChildren<Camera>();
		gui = GameObject.FindGameObjectWithTag ("GUI").GetComponent<GUIManager> ();
		visionSight = false;
		selectedPower = 1;
		gui.UpdateSelectedAbility (activePreceptImage, "Perception Dampening");
		foreach(GameObject r in GameObject.FindGameObjectsWithTag ("FoV"))
		{
			Renderer rend = r.GetComponent<Renderer>();
			FoVList.Add(rend);
			rend.enabled = false;
		}
		endurance = 100f;
	}

	void Update()
	{
		if(Time.timeScale == 1f)
		{
			GetInput ();
			ManageEndurance ();
		}
	}

	void GetInput()
	{
		if(Input.GetMouseButtonDown(0))
		{
			attackAnim.SetTrigger("ReadyAttack");
			attackAnim.SetFloat("EngageAttack", 0);
			attackAnim.SetFloat("CancelAttack", 0f);
		}

		else if(Input.GetMouseButtonDown(1))
		{
			attackAnim.SetFloat("CancelAttack", 1f);
		}

		else if(Input.GetMouseButtonUp(0))
		{
			attackAnim.SetFloat("EngageAttack", 1f);
		}

		if(Input.GetKey(KeyCode.Alpha1))
		{
			selectedPower = 1;
			gui.UpdateSelectedAbility (activePreceptImage, "Perception Dampening");
		}
		else if(Input.GetKey(KeyCode.Alpha2))
		{
			selectedPower = 2;
			gui.UpdateSelectedAbility (hallucinateImage, "Hallucinate");
		}

		if(overloadTime <= 0)
		{
			if(Input.GetMouseButtonDown(1) && !Input.GetMouseButton(0))
			{
				if(selectedPower == 1)
				{
					Ray r = new Ray(cam.transform.position, cam.transform.forward);
					RaycastHit hitInfo;
					if(Physics.Raycast(r, out hitInfo))
					{
						if(hitInfo.collider.tag == "Enemy")
						{
							if(activePercepTarget == null)
							{
								activePercepTarget = hitInfo.transform.GetComponent<BasicEnemy>();
								activePercepTarget.ActivelyDampened = true;
							}
							else
							{
								activePercepTarget.ActivelyDampened = false;
								activePercepTarget = hitInfo.transform.GetComponent<BasicEnemy>();
								activePercepTarget.ActivelyDampened = true;
							}
						}
						else
						{
							if(activePercepTarget != null)
							{
								activePercepTarget.ActivelyDampened = false;
								activePercepTarget = null;
							}
						}
					}
				}
				
				else if(selectedPower == 2)
				{
					if (endurance >= 25) {
						Ray r = new Ray (cam.transform.position, cam.transform.forward);
						RaycastHit hitInfo;
						if (Physics.Raycast (r, out hitInfo)) {
							Collider[] cols = Physics.OverlapSphere (hitInfo.point, 10, enemyMask);
							foreach (Collider c in cols) {
								BasicEnemy enemy = c.GetComponent<BasicEnemy> ();
								enemy.Investigate (hitInfo.point);
							}
						}
						endurance -= 25;
					} else
						Debug.Log ("Not Enough Endurance");
				}
			}
			
			if(Input.GetKeyDown(KeyCode.Q))
			{
				visionSight = !visionSight;
				foreach(Renderer r in FoVList)
				{
					if (visionSight)
					{
						r.enabled = true;
					}
					else
					{
						r.enabled = false;
					}
				}
			}
		}

		else
		{
			overloadTime -= Time.deltaTime;
		}

		if (Input.GetKeyDown (KeyCode.Alpha0))
			selectedPower = 0;
		else if (Input.GetKeyDown (KeyCode.Alpha1))
			selectedPower = 1;
	}

	void ManageEndurance()
	{
		float drainRate = 5;
		if(visionSight)
		{
			drainRate -= 10;
		}
		if(activePercepTarget != null)
		{
			drainRate -= 10;
		}
		if (endurance + drainRate <= 100)
			endurance += drainRate * Time.deltaTime;
		else
			endurance = 100;
		if(endurance <= 0)
		{
			endurance = 0;
			overloadTime = 3f;
			if(activePercepTarget != null)
			{
				activePercepTarget.ActivelyDampened = false;
				activePercepTarget = null;
			}
			if (visionSight)
			{
				visionSight = false;
				foreach(Renderer r in FoVList)
					r.enabled = false;
			}
		}
	}
}
