﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SecurityKeypad : MonoBehaviour {

	[SerializeField]Text ioText;
	[SerializeField]string lockID;
	[SerializeField]DoorScript door;
	Camera mainCamera, keypadCamera;
	string idInput;
	bool inUse;
	int numInputs;

	// Use this for initialization
	void Start () {
		mainCamera = Camera.main;
		keypadCamera = GetComponentInChildren<Camera>();
		keypadCamera.enabled = false;
		inUse = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(inUse)
		{
			if(Input.GetKeyDown(KeyCode.Escape))
			{
				Interact ();
			}
			if(Input.GetKeyDown(KeyCode.Alpha1))
			{
				numInputs++;
				idInput = idInput + "1";
				ioText.text = idInput;
			}

			else if(Input.GetKeyDown(KeyCode.Alpha2))
			{
				numInputs++;
				idInput = idInput + "2";
				ioText.text = idInput;
			}

			else if(Input.GetKeyDown(KeyCode.Alpha3))
			{
				numInputs++;
				idInput = idInput + "3";
				ioText.text = idInput;
			}

			else if(Input.GetKeyDown(KeyCode.Alpha4))
			{
				numInputs++;
				idInput = idInput + "4";
				ioText.text = idInput;
			}

			else if(Input.GetKeyDown(KeyCode.Alpha5))
			{
				numInputs++;
				idInput = idInput + "5";
				ioText.text = idInput;
			}

			else if(Input.GetKeyDown(KeyCode.Alpha6))
			{
				numInputs++;
				idInput = idInput + "6";
				ioText.text = idInput;
			}

			else if(Input.GetKeyDown(KeyCode.Alpha7))
			{
				numInputs++;
				idInput = idInput + "7";
				ioText.text = idInput;
			}

			else if(Input.GetKeyDown(KeyCode.Alpha8))
			{
				numInputs++;
				idInput = idInput + "8";
				ioText.text = idInput;
			}

			else if(Input.GetKeyDown(KeyCode.Alpha9))
			{
				numInputs++;
				idInput = idInput + "9";
				ioText.text = idInput;
			}

			else if(Input.GetKeyDown(KeyCode.Alpha0))
			{
				numInputs++;
				idInput = idInput + "0";
				ioText.text = idInput;
			}

			else if(Input.GetKeyDown(KeyCode.Backspace))
			{
				if(numInputs > 0)
				{
					idInput = idInput.Remove(numInputs-1);
					ioText.text = idInput;
					numInputs--;
				}
			}

			if(numInputs == lockID.Length)
			{
				if(idInput == lockID)
				{
					StartCoroutine ("AcessGranted", true);
				}
				else 
				{
					StartCoroutine ("AcessGranted", false);
				}
			}
		}
	}

	IEnumerator AcessGranted(bool granted)
	{
		if(granted)
		{
			ioText.color = Color.green;
			yield return new WaitForSeconds (.125f);
			ioText.text = "";
			yield return new WaitForSeconds (.125f);
			ioText.text = idInput;
			yield return new WaitForSeconds (.125f);
			ioText.text = "";
			yield return new WaitForSeconds (.125f);
			ioText.text = idInput;


			yield return new WaitForSeconds (.125f);
			ioText.text = "";
			yield return new WaitForSeconds (.125f);
			ioText.text = "Acess Granted";
			yield return new WaitForSeconds (.125f);
			ioText.text = "";
			yield return new WaitForSeconds (.125f);
			ioText.text = "Acess Granted";
			mainCamera.enabled = true;
			keypadCamera.enabled = false;
			inUse = false;
			NotificationCenter.Default.PostNotification("ExitMenu");
			door.CycleOpenState ();
		}

		else
		{
			ioText.color = Color.red;
			yield return new WaitForSeconds (.125f);
			ioText.text = "";
			yield return new WaitForSeconds (.125f);
			ioText.text = idInput;
			yield return new WaitForSeconds (.125f);
			ioText.text = "";
			yield return new WaitForSeconds (.125f);
			ioText.text = idInput;


			yield return new WaitForSeconds (.125f);
			ioText.text = "";
			yield return new WaitForSeconds (.125f);
			ioText.text = "Acess Denied";
			yield return new WaitForSeconds (.125f);
			ioText.text = "";
			yield return new WaitForSeconds (.125f);
			ioText.text = "Acess Denied";
		}

		numInputs = 0;
		idInput = "";
		ioText.text = idInput;
		ioText.color = Color.white;
	}

	public void Interact()
	{
		mainCamera.enabled = !mainCamera.enabled;
		keypadCamera.enabled = !keypadCamera.enabled;
		inUse = !inUse;
		ioText.text = "";
		idInput = "";
		numInputs = 0;
	}
}
