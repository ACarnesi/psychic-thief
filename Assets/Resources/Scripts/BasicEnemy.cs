﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BasicEnemy : MonoBehaviour {
	public List<Vector3> patrolPath = new List<Vector3>();
	public float fieldOfView = 110;
	public float speed;
	public bool activelyDampened;
	public Image alertImage;
	public Sprite searchSprite, alertSprite;
	public float searchTime, chaseTime, pauseTime;
	public int patrolIndex;
	public float distToSeePlayer;

	private Renderer rend;
	[SerializeField]private float timeSearched, timeChased, timePaused;
	private Vector3 lastPlayerPos, searchDir, startPos, startRot;
	private PlayerVisibility playerVis; 
	private NavMeshAgent navAgent;
	[SerializeField]private EnemyBehaviour enemyBehaviour;
	[SerializeField]private GameObject FoVObject;
	private Transform player;

	public bool ActivelyDampened{
		get{
			return activelyDampened;
		}
		set
		{
			activelyDampened = value;
			if(value == true)
			{
				rend.material.color = Color.blue;
			}
			else
				rend.material.color = Color.red;
		}
	}

	public enum EnemyBehaviour{
		OnDuty,
		OnSearch,
		OnPursuit,
		KnockedOut
	};

	void Start()
	{
		alertImage.enabled = false;
		rend = GetComponent<Renderer>();
		rend.material.color = Color.red;
		playerVis = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerVisibility>();
		player = GameObject.FindGameObjectWithTag ("Player").transform.GetChild (0);
		patrolIndex = 0;
		searchDir = Vector3.zero;
		timeSearched = 0;
		timeChased = 0;
		enemyBehaviour = EnemyBehaviour.OnDuty;
		startPos = transform.position;
		startRot = transform.rotation.eulerAngles;
		navAgent = GetComponent<NavMeshAgent>();
	}

	void Update()
	{
		if(Time.timeScale == 1)
		{
			if(enemyBehaviour != EnemyBehaviour.KnockedOut)
			{
				if(!activelyDampened)
					distToSeePlayer = Mathf.Lerp(distToSeePlayer, playerVis.PlayerVis / 10, Time.deltaTime * 50);
				else
				{
					distToSeePlayer = Mathf.Lerp(distToSeePlayer, playerVis.PlayerVis / 100, Time.deltaTime * 50);
				}
				if(Time.timeScale > .1)
				{
					FoVObject.transform.localScale = Vector3.one*distToSeePlayer;
				}
				
				float playerDist = Vector3.Distance(player.position, transform.position);
				//Check for player
				if(playerDist < distToSeePlayer)
				{
					Vector3 targetDir = player.position - transform.position;
					float angle = Vector3.Angle(transform.forward, targetDir);
					if(angle < fieldOfView * .5f)
					{
						RaycastHit hitInfo;
						if(Physics.Raycast(transform.position, targetDir, out hitInfo))
						{
							if(hitInfo.transform.tag == "Player")
							{
								timeChased = 0;
								lastPlayerPos = player.position;
								enemyBehaviour = EnemyBehaviour.OnPursuit;
							}
						}
					}
				}
				
				if(enemyBehaviour == EnemyBehaviour.OnDuty)
					OnDuty();
				
				else if(enemyBehaviour == EnemyBehaviour.OnSearch)
					OnSearch();
				
				else if(enemyBehaviour == EnemyBehaviour.OnPursuit)
					OnPursuit();
			}
		}
	}

	private void OnCollisionEnter(Collision col)
	{
		if(enemyBehaviour != EnemyBehaviour.KnockedOut)
		{
			if (col.collider.tag == "Player")
			{
				UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
			}
		}
	}

	void OnDrawGizmos()
	{
		for(int i = 1; i < patrolPath.Capacity; i++)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawLine(patrolPath[i - 1], patrolPath[i]);
		}
	}

	void OnDuty()
	{
		//Go to next Destination
		if(patrolPath.Capacity > 0)
		{
			if (timePaused <= 0) {
				navAgent.SetDestination (patrolPath [patrolIndex]);

				//If reached next Destination, start moving to the next destination. 
				if (transform.position.x == patrolPath [patrolIndex].x && transform.position.z == patrolPath [patrolIndex].z) {
					int lastPatrolIndex = patrolIndex;
					patrolIndex++;
					//Resets to starting position of patrol.
					if (patrolIndex >= patrolPath.Capacity)
						patrolIndex = 0;
					
					if (patrolPath [patrolIndex] == patrolPath[lastPatrolIndex]) {
						navAgent.Stop ();
						timePaused = pauseTime;
					}
					else
					{
						navAgent.Resume ();
						navAgent.SetDestination (patrolPath [patrolIndex]);
					}
				}
			} else
				timePaused -= Time.deltaTime;

		}
		else
		{
			if(transform.position.x != startPos.x && transform.position.z != startPos.z)
			{
				navAgent.SetDestination(startPos);
			}
			else
			{
				if(transform.rotation != Quaternion.Euler(startRot))
				{
					transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(startRot), 60 * Time.deltaTime);
				}
			}
		}
	}

	void OnSearch()
	{
		alertImage.sprite = searchSprite;
		alertImage.enabled = true;
		navAgent.Resume ();
		if(timeSearched < searchTime)
		{
			timeSearched += Time.deltaTime;
			//pick random direction and move in that direction. Repeat once you you finish moving. 
			if(searchDir == Vector3.zero)
			{
				searchDir = (new Vector3(transform.position.x + Random.Range(-1,1), transform.position.y, transform.position.z +  Random.Range(-1,1)) - transform.position).normalized;
				Ray r = new Ray(transform.position, searchDir);
				RaycastHit hitInfo;
				if(Physics.Raycast(r, out hitInfo))
				{
					navAgent.SetDestination(hitInfo.point);
				}
			}
			else if(transform.position.x == navAgent.destination.x && transform.position.z == navAgent.destination.z)
			{
				searchDir = (new Vector3(transform.position.x + Random.Range(-1,1), transform.position.y, transform.position.z +  Random.Range(-1,1)) - transform.position).normalized;
				Ray r = new Ray(transform.position, searchDir);
				RaycastHit hitInfo;
				if(Physics.Raycast(r, out hitInfo))
				{
					navAgent.SetDestination(hitInfo.point - searchDir*2);
				}
			}
		}
		else
		{
			timeSearched = 0;
			searchDir = Vector3.zero;
			alertImage.enabled = false;
			enemyBehaviour = EnemyBehaviour.OnDuty;
		}
	}

	void OnPursuit()
	{
		alertImage.sprite = alertSprite;
		alertImage.enabled = true;
		if(timeChased < chaseTime)
		{
			timeChased += Time.deltaTime;
			navAgent.SetDestination(player.position);
			lastPlayerPos = player.position;
		}
		else
		{
			if(transform.position.x != lastPlayerPos.x && transform.position.z != lastPlayerPos.z)
			{
				navAgent.SetDestination(lastPlayerPos);
			}
			else
			{
				timeChased = 0;
				alertImage.enabled = false;
				enemyBehaviour = EnemyBehaviour.OnSearch;
			}
		}
	}

	IEnumerator FallOver()
	{
		Quaternion endRot = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z + 90);
		while(transform.rotation != endRot)
		{
			transform.rotation = Quaternion.RotateTowards(transform.rotation, endRot, 75 * Time.deltaTime);
			yield return null;
		}
//		transform.Rotate(Vector3.forward, 90);
					yield return null;
	}

	public void Investigate(Vector3 point)
	{
		if(enemyBehaviour != EnemyBehaviour.OnPursuit || enemyBehaviour != EnemyBehaviour.KnockedOut)
		{
			enemyBehaviour = EnemyBehaviour.OnSearch;
			searchDir = (new Vector3(point.x, transform.position.y, point.z) - transform.position).normalized;
			
			navAgent.SetDestination(point - searchDir*2);
		}
	}

	public void Alert()
	{
		if(enemyBehaviour != EnemyBehaviour.KnockedOut)
		{
			navAgent.Resume ();
			lastPlayerPos = player.position;
			enemyBehaviour = EnemyBehaviour.OnPursuit;
		}
	}

	public void Attacked()
	{
		if(enemyBehaviour != EnemyBehaviour.KnockedOut)
		{
			if(enemyBehaviour != EnemyBehaviour.OnPursuit)
			{
				enemyBehaviour = EnemyBehaviour.KnockedOut;
				navAgent.enabled = false;
				alertImage.enabled = false;
				GetComponent<Rigidbody> ().useGravity = true;
				StartCoroutine("FallOver");
			}
		}
	}
}